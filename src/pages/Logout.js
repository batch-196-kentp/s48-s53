import {useContext,  useEffect} from 'react';
import UserContext from '../UserContext'
import {Navigate} from 'react-router-dom'


export default function Logout(){
	// localStorage.clear();


	
	//clear the localStorage of the user's information. //from provider
	const {unsetUser, setUser} = useContext(UserContext);

	unsetUser();

	//Placing the setUser function inside of a useEffect is necessary because of updates with in ReactJS that a sate of another co mponent cannot be updated while trying to render a different component

	//By adding useEffect, this will allow the logout page to render first beofre trigerring the useEffect which changes the state of the user.
	useEffect(()=> {
		//set the user state back to it's original state
		setUser({id:null})
	});


	return(
		<Navigate to='/login'/>


	)

}