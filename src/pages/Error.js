import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
export default function Error(){
	return(
		<Row>
			<Col className ="p-5 fw-bold">
				<h1 variant="secondary">404 Not Found</h1>
				<p>Page not found!</p>
				<Button variant="primary" as={Link} to='/'>Back Home</Button>
			</Col>
		</Row>
	)
}

