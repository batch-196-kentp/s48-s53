import {useState, useEffect,useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import UserContext from '../UserContext'
import {Navigate, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'


export default function Register(){

	const {user} = useContext(UserContext);
	const history = useNavigate();
	//state hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] =useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('');
	// const [password2, setPassword2] = useState('');
	//state to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);
	// console.log(email);
	// console.log(password1);
	// console.log(password2);

	function registerUser(e){
		//prevents page redirection via form submission
		e.preventDefault();


		fetch('http://localhost:4000/users/checkEmailExists', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			//result: boolean

			if(data){
				Swal.fire({
					title: 'Duplicate email found!',
					icon: 'info',
					text: 'Email already registered.'
				})
			} else {
				fetch('http://localhost:4000/users', {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password,
						mobileNo: mobileNo
					})

				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data.email){
						Swal.fire({
							title: 'Registration successful!',
							icon: 'success',
							text: 'Thank your registering'
						})
						history("/login")
					} else {
						Swal.fire({
							title: 'Registration failed',
							icon: 'error',
							text: 'Something went wrong, please try again.'
						})
					}
				})
			}
		})

		//clear input fields
		// setEmail('');
		// setPassword('');
		// setMobileNo('');
		// setFirstName('');
		// setLastName('')

		// alert("Thank you for registering!");
	}
	
	//Syntax:
		//useEffect(() => {}, [])

	useEffect(() => {
		//validation to enable the submit button when all fields are populated and both passwords match
		if(email !== '' && firstName !== '' && lastName !== '' && password !== '' && mobileNo !== '' && mobileNo.length === 11){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, firstName, lastName, password, mobileNo]);

	return(
		(user.id !== null) ?
			<Navigate to='/courses'/>
		:
		<>
		<h1>Register Here:</h1>
		<Form onSubmit={e => registerUser(e)}>

			<Form.Group controlId="userEmail">

			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control 
				type="text" 
				placeholder="Please input your first name here" 
				required value={firstName} 
				onChange={e=> setFirstName(e.target.value)}/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
				type="text" 
				placeholder="Please input your last name here" 
				required value={lastName} 
				onChange={e=> setLastName(e.target.value)}/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile No</Form.Label>
				<Form.Control  
				placeholder="Please input your 11-digit mobile number here" 
				required value={mobileNo} 
				onChange={e=> setMobileNo(e.target.value)}/>
			</Form.Group>

				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email" 
					placeholder="Enter your email here" 
					required 
					value={email} 
					onChange={e=> setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control 
				type="password" 
				placeholder="Enter your password here" 
				required value={password} 
				onChange={e=> setPassword(e.target.value)}/>
			</Form.Group>




		{ isActive ?			
			<Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
				Register
			</Button>
			:
			<Button variant="secondary" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
				Register
			</Button>
		}
		
		</Form>
		</>

	)
}