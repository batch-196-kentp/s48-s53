import {useState} from 'react';
import {Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom';

export default function CourseCard({courseProp}) {
	//check to see if data was successfully passed
	//console.log(props);//result: php-laravel (coursesData[0])
	//every component receive information in a form of object
	//console.log(typeof props);
	//object destructuring
	const {name, description, price, _id} = courseProp;

	//react hooks - useState -> store its state
	//Syntax:
		//const [getter,setter] = useState(initialGetterValue);
	// const [count, setCount] = useState(10);
	// const [enrollee, setEnrollee] =useState(0);
	// console.log(useState(0));

	// function enroll(){
	// 	if(count === 0){
	// 		alert("No more seats available, check back later.");
	// 	} else {
	// 		setCount(count -1);
	// 		setEnrollee(enrollee + 1);
	// 	}
	// 	console.log(`Enrollees: ${enrollee}`)
	// 	console.log(`Seats: ${count}`)
	// }

	return (
				<Card className="mb-1">
					<Card.Body>
						<Card.Title className="fw-bold">
							<h4>{name}</h4>
						</Card.Title>
						<Card.Subtitle> Course Description:</Card.Subtitle>
						<Card.Text>
								{description}
						</Card.Text>
						<Card.Subtitle>
							Course Price:
						</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
					</Card.Body>
				</Card>
	)
};